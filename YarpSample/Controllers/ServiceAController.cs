using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace YarpSample.Controllers;
[ApiVersion(1.0)]
[ApiController]
[Route("/api/serviceA")]
public class ServiceAController : ControllerBase
{

    private readonly ILogger<ServiceAController> _logger;

    public ServiceAController(ILogger<ServiceAController> logger)
    {
        _logger = logger;
    }

    [HttpPost(Name = "PostServiceA")]
    public string Post()
    {
        return "Post service A";
    }
    //[Route("{**catch-all}", Name = "CatchAllServiceA")]
    //public string CatchAll()
    //{
    //    return "Catch all service A";
    //}
}