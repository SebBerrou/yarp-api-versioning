using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;

namespace YarpSample.Controllers;
[ApiVersion(1.0)]
[ApiController]
[Route("/api/serviceB")]
public class ServiceBController : ControllerBase
{

    private readonly ILogger<ServiceBController> _logger;

    public ServiceBController(ILogger<ServiceBController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetServiceB")]
    public string Get()
    {
        return "Get service B";
    }
    [HttpPost(Name = "PostServiceB")]
    public string Post()
    {
        return "Post service B";
    }
}