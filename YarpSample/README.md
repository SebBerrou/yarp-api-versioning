
Call on the route http://localhost:5002/api/v1/serviceA/ should return `504 Bad Gateway`, with logs :
```
// Logs
Hôte inconnu. (servicea.api:80)
```
But when adding .AddMvc() from api version
```csharp
builder.Services.AddApiVersioning(options =>
{
    options.ReportApiVersions = true;
    options.AssumeDefaultVersionWhenUnspecified = true;
}).AddMvc();
```
We have an `405 Method Not Allowed` result

Calling http://localhost:5002/api/v1/serviceA/other is working fine
