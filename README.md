L'appel sur la route http://localhost:5002/api/v1/serviceA/ doit renvoyer un `504 Bad Gateway`
```
// Logs
Hôte inconnu. (servicea.api:80)
```
Mais si on ajoute les services Infineoapi dans le Program.cs
```csharp
builder.AddInfineoApi(opt =>
{
    opt.AppName = "YarpSample";
    opt.Authentication.Enable = false;

});
```
On a en retour du même appel une erreur `405 Method Not Allowed`
